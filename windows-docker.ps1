$LOGFILE = "c:\logs\windows-docker.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start windows-docker"
  try {
    $url = "https://raw.githubusercontent.com/microsoft/Windows-Containers/Main/helpful_tools/Install-DockerCE/install-docker-ce.ps1"
    $outpath = "$env:temp\install-docker-ce.ps1"
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile($url, $outpath)

    . "$env:temp\install-docker-ce.ps1"

  }
  catch {
      Write-Log "$_"
      Throw $_
  }
  

  Write-Log "End  windows-docker"
 
}

Main 
